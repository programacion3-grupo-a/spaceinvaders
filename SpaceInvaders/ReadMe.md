# Proyecto de Curso: "Space Invaders"

Bienvenido al proyecto de curso "Space Invaders". Este proyecto tiene como objetivo principal la creación de una estrategia de testeo para una versión desktop de la clásica recreación del juego "Space Invaders".

## Objetivo del Juego

El objetivo del juego es demostrar el uso de componentes visuales e interactuar mediante eventos, manejar procesos (movimientos) manteniendo responsive el uso de la aplicación, además del uso de archivos como mecanismos para guardar y cargar información.

## Requerimientos de Desarrollo

El juego permitirá controlar una nave que contiene un láser, esta puede disparar contra las olas de naves alienígenas de esa manera logrando incrementar el score. Los elementos y ponderaciones para incrementar el score se detallan a continuación.

### Movimiento del Jugador

- El jugador podrá desplazarse de izquierda a derecha presionando las teclas de dirección flecha a la izquierda y derecha, respectivamente.
- El botón de Espacio (Space) será utilizado para atacar.

### Movimiento de las Naves Alienígenas

- Se mueven de izquierda a derecha, una vez llegado al borde, se desplazan hacia abajo una posición y se mueven al borde opuesto.
- Cada vez que existe un desplazamiento hacia abajo, la velocidad de movimiento incrementa un poco, al igual que la velocidad del disparo de ellas.
- La única nave que ataca es la que vale 40 PTS.

### Naves Alienígenas Rojas

- Se mueven aleatoriamente empezando de izquierda a derecha o viceversa y saliendo del marco, aparecen cada cierto tiempo, aproximadamente 1 vez por 2 minutos.
- Los puntos que se obtienen al dispararles varían.

### Fin del Juego

El juego terminará cuando:

- Las vidas del jugador se agoten.
- Las naves alienígenas alcancen al jugador.

### Elementos Importantes

- Existirán 4 bloques de protección que ayudarán al jugador con los disparos de los alienígenas.
- Los bloques de protección se desgastan cada vez que impacta un disparo sea de una nave alienígena o del jugador.
- El jugador puede disparar nuevamente cuando su disparo anterior impacte o sobrepase el límite superior del marco del juego.
- Los alienígenas no pueden colisionar con los bloques de protección.
- Las naves alienígenas se destruyen con 1 disparo.
- Cuando todas las naves alienígenas sean destruidas, una nueva ola se genera moviendo la posición abajo para las primeras 5 e incrementando levemente la velocidad a partir de la quinta en adelante.
- Cada 1000 puntos se incrementa en uno el número de vidas, hasta llegar a un máximo de 6.

### Acerca del Score

- El score se muestra en la esquina superior izquierda.
- Se incrementa siguiendo la tabla mostrada previamente cada vez que un alienígena es destruido.

### Después de que el Juego Finaliza

- Se da la opción al jugador de guardar su score, agregando un seudónimo.
- Se da la opción al jugador de volver a jugar.
- Si el jugador no quisiera volver a jugar, entonces se muestra la página de inicio.

### Pantalla de Inicio

La pantalla de inicio dará las siguientes opciones:

- Iniciar una nueva partida.
- Ver el score board.
- Ver los controles del juego.

Cada acción en el juego deberá estar acompañada de un sonido representativo. La información del score board deberá ser guardada en un archivo de texto.

---

Este proyecto busca no solo implementar un juego clásico sino también destacar la aplicación de conceptos clave en desarrollo de software, como manejo de eventos, interactividad, gestión de procesos y persistencia de datos. ¡Disfruta del desarrollo y la implementación de tu versión de "Space Invaders"!
