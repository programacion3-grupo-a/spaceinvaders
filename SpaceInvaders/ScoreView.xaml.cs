namespace SpaceInvaders;
using SpaceInvaders.Model;

public sealed partial class ScoreView : Page
{
    public ScoreView()
    {
        this.InitializeComponent();

        List<string> scores = ScoreManager.ReadScores();
        foreach (string score in scores)
        {
            ShowScore(score);
        }
    }

    public void ShowScore(string score)
    {
        TextBlock textBlock = new TextBlock();
        textBlock.Text = score;
        textBlock.Style = (Style)Resources["ListTextStyle"];
        textBlock.Margin = new Thickness(0, 0, 0, 40);
        scoresPanel.Children.Add(textBlock);
    }

    private void RegresarAtras_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(InicioSesion));
    }
}
