using System;
using System.Collections.Generic;

namespace SpaceInvaders;
/// <summary>
/// An empty page that can be used on its own or navigated to within a Frame.
/// </summary>
public sealed partial class Controles : Page
{
    public Controles()
    {
        this.InitializeComponent();
    }

    private void RegresarAtras_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(InicioSesion));
    }
}
