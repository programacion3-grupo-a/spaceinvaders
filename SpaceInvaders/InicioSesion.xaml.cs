
namespace SpaceInvaders;

public sealed partial class InicioSesion : Page
{
    public InicioSesion()
    {
        this.InitializeComponent();

    }

    private void Jugar_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(MainPage));
    }

    private void Score_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(ScoreView));
    }

    private void ManualJuego_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(Controles));
    }
}
