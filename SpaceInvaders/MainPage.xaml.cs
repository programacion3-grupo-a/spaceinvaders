namespace SpaceInvaders;
 using Microsoft.UI.Xaml.Input;
using SpaceInvaders.Model.Utils;
using SpaceInvaders.Model;
using Windows.Media.Core;

public sealed partial class MainPage : Page
{
    private Juego juego;
    private Jugador jugador;
    private ICanvas canvas;
    private ICollisionDetector collisionDetector;

    public MainPage()
    {
        this.InitializeComponent();
        this.KeyDown += MainPage_KeyDown;
        canvas = new CanvasDibujar(canvasJuego);
        jugador = new Jugador(canvas);
        juego = new Juego(canvas);
        collisionDetector = new CollisionDetector(juego.Muros, juego.NavesAlienigenas, juego.NaveAlienigenaRoja, jugador.Nave);
        juego.CollisionDetector = collisionDetector;
        juego.movimientoNavesAlienigenas.TerminarJuegoRequested += Juego_TerminarJuegoRequested;
        juego.movimientoNavesAlienigenas.ReproducirSonido += Reproducir_Sonido;
        collisionDetector.VidasActualizadas += Jugador_VidasActualizadas;
        collisionDetector.PuntajeActualizado += Jugador_PuntajeActualizado;
        collisionDetector.ReproducirSonido += Reproducir_Sonido;

        Jugador_VidasActualizadas(this, jugador.Nave.Vidas);
        jugador.ReproducirSonido += Reproducir_Sonido;
    }

    private void CanvasJuego_PointerPressed(object sender, PointerRoutedEventArgs e)
    {
        canvasJuego.Focus(FocusState.Programmatic);
    }

    private void MainPage_KeyDown(object sender, KeyRoutedEventArgs e)
    {
        switch (e.Key)
        {
            case Windows.System.VirtualKey.Left:
                jugador.MoverNaveIzquierda(canvas.CanvasAncho);
                break;
            case Windows.System.VirtualKey.Right:
                jugador.MoverNaveDerecha(canvas.CanvasAncho);
                break;
            case Windows.System.VirtualKey.Space:
                jugador.DispararBala(collisionDetector);
                break;
        }
    }

    private void Juego_TerminarJuegoRequested(object sender, EventArgs e)
    {
        Frame.Navigate(typeof(SaveScore), jugador.Nave.Puntos);
    }

    private void Jugador_VidasActualizadas(object sender, int vidas)
    {
        imagenStackPanel.Children.Clear();
        vidas = Math.Min(vidas, 6);
        for (int i = 0; i < vidas; i++)
        {
            Image nuevaImagen = ImageConstants.Vida;
            imagenStackPanel.Children.Add(nuevaImagen);
        }

        if (vidas == 0)
        {
            Juego_TerminarJuegoRequested(this, EventArgs.Empty);
        }
    }

    private void Jugador_PuntajeActualizado(object sender, int puntaje)
    {
        score.Text = puntaje.ToString();
        int puntosPorVida = 1000;
        int vidasExtras = puntaje / puntosPorVida;

        if (vidasExtras > jugador.Nave.Vidas)
        {
            int vidasGanadas = vidasExtras - jugador.Nave.Vidas;
            jugador.Nave.Vidas += vidasGanadas;
            Jugador_VidasActualizadas(this, jugador.Nave.Vidas);
        }
    }

    private void Reproducir_Sonido(object sender, string rutaSonido)
    {
        soundPlayer.MediaPlayer.Pause();
        MediaSource mediaSource = MediaSource.CreateFromUri(new Uri(rutaSonido));
        soundPlayer.MediaPlayer.Source = mediaSource;
        soundPlayer.MediaPlayer.PlaybackSession.Position = TimeSpan.Zero;
        soundPlayer.MediaPlayer.Play();
    }
}
