using SpaceInvaders.Model.Utils;
namespace SpaceInvaders.Model;

public class Muro
{
    public List<Bloque> Bloques { get; set; }
    public double PosicionX { get; set; }
    public double PosicionY { get; set; }
    public ICanvas canvas;

    public Muro(ICanvas canvas)
    {
        this.canvas = canvas;
        Bloques = new List<Bloque>
            {
                new Bloque(ImageConstants.Muro, 5),
                new Bloque(ImageConstants.Muro, 5),
                new Bloque(ImageConstants.Muro, 5),
                new Bloque(ImageConstants.Muro, 5),
                new Bloque(ImageConstants.Muro, 5)
            };
    }

    public bool ColisionConBala(double balaLeft, double balaTop, double balaWidth, double balaHeight,bool esJugador)
    {
        foreach (Bloque bloque in Bloques)
        {
            double bloqueLeft = Canvas.GetLeft(bloque.Imagen);
            double bloqueTop = Canvas.GetTop(bloque.Imagen);
            double bloqueWidth = bloque.Imagen.ActualWidth;
            double bloqueHeight = bloque.Imagen.ActualHeight;

            if (balaLeft < bloqueLeft + bloqueWidth &&
                balaLeft + balaWidth > bloqueLeft &&
                balaTop < bloqueTop + bloqueHeight &&
                balaTop + balaHeight > bloqueTop)
            {
                if (bloque.ReducirResistencia(esJugador))
                {
                    Bloques.Remove(bloque);
                    canvas.EliminarImagen(bloque.Imagen);
                }
                return true;
            }
        }
        return false;
    }

    public void DibujarMuros()
    {
        const double tamañoBloqueEnPixeles = 25;
        const int bloquesPorFila = 3;
        const int filaCentral = 1;
        const int mitadBloquesPorFila = bloquesPorFila / 2;

        double posicionInicialX = PosicionX + (tamañoBloqueEnPixeles * bloquesPorFila);
        double posicionInicialY = PosicionY;

        for (int indiceBloque = 0; indiceBloque < Bloques.Count; indiceBloque++)
        {
            int columna = indiceBloque % bloquesPorFila;
            int fila = indiceBloque / bloquesPorFila;

            if (fila == filaCentral && columna == mitadBloquesPorFila)
            {
                posicionInicialX += tamañoBloqueEnPixeles;
            }

            double posicionX = posicionInicialX + columna * tamañoBloqueEnPixeles;
            double posicionY = posicionInicialY + fila * tamañoBloqueEnPixeles;

            canvas.DibujarImagen(Bloques[indiceBloque].Imagen, posicionX, posicionY);
        }
    }
}
