namespace SpaceInvaders.Model.Utils;

public static class SoundConstants
{
    public const string ShootSound = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Assets\\Audio\\prueba.wav";
    public const string Disparo = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Assets\\Audio\\shoot.wav";
    public const string Explosion = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Assets\\Audio\\explosion.wav";
    public const string ExplosionNave = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Assets\\Audio\\explosionNaveEnemiga.wav";
    public const string MovimientoNave = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Assets\\Audio\\movimientoNave.wav";
    public const string GameOver = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Assets\\Audio\\gameover.wav";

}
