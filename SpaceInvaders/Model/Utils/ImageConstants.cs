using Microsoft.UI.Xaml.Media.Imaging;

namespace SpaceInvaders.Model.Utils;
public static class ImageConstants
{
    public static Image NaveEnemigaAtacante => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Nave/naveAtacante.jpeg", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image NaveEnemiga20 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Nave/navaEnemiga20.jpeg", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image NaveEnemiga10 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Nave/naveEnemiga10.jpeg", 25, 25, new Thickness(0, 0, 0, 0));

    public static Image JugadorBloque_1 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/jugadorBloque_1.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image JugadorBloque_2 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/jugadorBloque_2.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image JugadorBloque_3 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/jugadorBloque_3.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image JugadorBloque_4 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/jugadorBloque_4.png", 25, 25, new Thickness(0, 0, 0, 0));
    
    public static Image EnemigoBloque_1 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/enemigoBloque_1.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image EnemigoBloque_2 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/enemigoBloque_2.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image EnemigoBloque_3 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/enemigoBloque_3.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image EnemigoBloque_4 => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/Bloques/enemigoBloque_4.png", 25, 25, new Thickness(0, 0, 0, 0));

    public static Image NaveRojaDestruida => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/naveRojaDestruida.png", 60, 30, new Thickness(0, 0, 0, 0));
    public static Image NaveRoja => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/naveRoja.png", 60, 30, new Thickness(0, 0, 0, 0));
    public static Image Muro => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/muroPrincipal.png", 25, 25, new Thickness(0, 0, 0, 0));
    public static Image Jugador => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/naveV.png", 80, 40, new Thickness(0, 0, 0, 0));
    public static Image Bala => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/bala.png", 20, 20, new Thickness(0, 0, 10, 0));
    public static Image Vida => CreateImage("ms-appx:///SpaceInvaders/Assets/Images/naveV.png", 30, 30, new Thickness(0, 0, 10, 0));

    private static Image CreateImage(string uri, double width, double height, Thickness margin)
    {
        Image image = new Image
        {
            Source = new BitmapImage(new Uri(uri)),
            Width = width,
            Height = height,
            Margin = margin
        };

        return image;
    }
}
