using SpaceInvaders.Model.Utils;
namespace SpaceInvaders.Model;

public class Bala
{
    public Image Sprite { get; } = ImageConstants.Bala;
    public double PosicionX { get; set; }
    public double PosicionY { get; set; }
    public int Velocidad { get; set; }
    public bool EstaViva { get; set; }
    public TipoBala Origen { get; }
    private DispatcherTimer timer;
    public ICollisionDetector collisionDetector;
    public ICanvas canvas;

    public Bala(ICanvas canvas, ICollisionDetector collisionDetector, double posicionX, double posicionY, int velocidad, TipoBala origen)
    {
        this.canvas = canvas;
        this.collisionDetector = collisionDetector;
        PosicionX = posicionX;
        PosicionY = posicionY;
        Velocidad = velocidad;
        EstaViva = true;
        Origen = origen;

        timer = new DispatcherTimer();
        timer.Interval = TimeSpan.FromMilliseconds(16);
        timer.Tick += Timer_Tick;
        timer.Start();
    }

    private void Timer_Tick(object sender, object e)
    {
        if (EstaViva)
        {
            if (Origen == TipoBala.NaveEspacial)
            {
                MoverArriba();
            }
            else
            {
                MoverAbajo();
            }
            VerificarColision();
        }
    }

    private void VerificarColision()
    {
            Canvas.SetTop(Sprite, PosicionY);

            double balaLeft = Canvas.GetLeft(Sprite);
            double balaTop = Canvas.GetTop(Sprite);
            double balaWidth = Sprite.ActualWidth;
            double balaHeight = Sprite.ActualHeight;

            if (collisionDetector.DetectarColision(Origen, balaLeft, balaTop, balaWidth, balaHeight))
            {
                canvas.EliminarImagen(Sprite);
                EstaViva = false;
            }
    }

    public void MoverArriba()
    {
        double distanciaDesdeBordeSuperior = 50;
        if (PosicionY < distanciaDesdeBordeSuperior)
        {
            canvas.EliminarImagen(Sprite);
            EstaViva = false;
        }
        PosicionY -= Velocidad;
        canvas.MoverImagen(Sprite, PosicionX, PosicionY);
    }

    public void MoverAbajo()
    {
        if (PosicionY > canvas.CanvasAlto - 20)
        {
            canvas.EliminarImagen(Sprite);
            EstaViva = false;
        }
        PosicionY += Velocidad;
        canvas.MoverImagen(Sprite, PosicionX, PosicionY);
    }
}
