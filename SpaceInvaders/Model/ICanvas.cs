namespace SpaceInvaders.Model;

public interface ICanvas
{
    double CanvasAncho { get; }
    double CanvasAlto { get; }
    void DibujarImagen(Image imagen, double posicionX, double posicionY);
    void EliminarImagen(Image imagen);
    void MoverImagen(Image imagen, double nuevaPosicionX, double nuevaPosicionY);
}
