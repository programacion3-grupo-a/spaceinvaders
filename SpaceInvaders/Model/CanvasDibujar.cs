namespace SpaceInvaders.Model;

public class CanvasDibujar : ICanvas
{
    private readonly Canvas canvas;
    public double CanvasAncho => canvas.Width;
    public double CanvasAlto => canvas.Height;

    public CanvasDibujar(Canvas canvas)
    {
        this.canvas = canvas;
    }

    public void DibujarImagen(Image imagen, double posicionX, double posicionY)
    {
        Canvas.SetLeft(imagen, posicionX); 
        Canvas.SetTop(imagen, posicionY); 
        canvas.Children.Add(imagen); 
    }

    public void EliminarImagen(Image imagen)
    {
        canvas.Children.Remove(imagen);
    }

    public void MoverImagen(Image imagen, double nuevaPosicionX, double nuevaPosicionY)
    {
        Canvas.SetLeft(imagen, nuevaPosicionX);
        Canvas.SetTop(imagen, nuevaPosicionY);
    }
}
