namespace SpaceInvaders.Model;

public class Nave : IMovible
{
    public Image Sprite { get; set; }
    public double PosicionX { get; set; }
    public double PosicionY { get; set; }
    public double Velocidad { get; set; }
    public bool EstaViva { get; set; }
    public int Puntos { get; set; }
    public ICanvas canvas;

    public Nave(ICanvas canvas, Image sprite, double posicionX, double posicionY, double velocidad, int puntos)
    {
        this.canvas = canvas;
        Sprite = sprite;
        PosicionX = posicionX;
        PosicionY = posicionY;
        Velocidad = velocidad;
        EstaViva = true;
        Puntos = puntos;
    }

    public virtual void Mover()
    {
        canvas.MoverImagen(Sprite, PosicionX, PosicionY);
    }

    public virtual void Dibujar()
    {
        canvas.DibujarImagen(Sprite, PosicionX, PosicionY);
    }

    public virtual void Eliminar()
    {
        canvas.EliminarImagen(Sprite);
    }

    public bool VerificarColisiones(double balaLeft, double balaTop, double balaWidth, double balaHeight)
    {
        double naveLeft = PosicionX;
        double naveTop = PosicionY;
        double naveWidth = Sprite.ActualWidth;
        double naveHeight = Sprite.ActualHeight;

        return balaLeft + balaWidth > naveLeft && balaLeft < naveLeft + naveWidth &&
               balaTop + balaHeight > naveTop && balaTop < naveTop + naveHeight;
    }
}
