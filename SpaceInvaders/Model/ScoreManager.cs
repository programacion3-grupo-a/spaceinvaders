using System.Text;

namespace SpaceInvaders.Model;

public class ScoreManager
{
    private static readonly string filePath = "C:\\Users\\EverMamaniV\\source\\repos\\SpaceInvaders\\SpaceInvaders\\Files\\scores.txt";
  
    public static void SaveScore(string playerName, string score)
    {
        try
        {
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine($"{playerName},{score}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error al guardar el puntaje: {ex.Message}");
        }
    }

    public static List<string> ReadScores()
    {
        List<string> scoresList = new List<string>();
        try
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    scoresList.Add(line);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error al leer los puntajes: {ex.Message}");
        }
        return scoresList;
    }
}
