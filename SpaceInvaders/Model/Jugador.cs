using SpaceInvaders.Model.Utils;
namespace SpaceInvaders.Model;

public class Jugador
{
    public event EventHandler<String> ReproducirSonido;
    public int vidas = 6;
    public int puntaje = 0;
    public int velocidad = 5;

    public NaveEspacial Nave { get; }
    public ICanvas canvas;

    public Jugador(ICanvas canvas)
    {
        this.canvas = canvas;

        double posicionInicialX = (canvas.CanvasAncho - ImageConstants.Jugador.Width) / 2;
        double posicionInicialY = canvas.CanvasAlto - ImageConstants.Jugador.Height;

        Nave = new NaveEspacial(canvas, posicionInicialX, posicionInicialY, velocidad, vidas, puntaje);
        canvas.DibujarImagen(Nave.Sprite, Nave.PosicionX, Nave.PosicionY);
    }

    public void MoverNaveIzquierda(double canvasWidth)
    {
        Nave.MoverIzquierda(canvasWidth);
        ReproducirSonido?.Invoke(this, SoundConstants.MovimientoNave);
    }

    public void MoverNaveDerecha(double canvasWidth)
    {
        Nave.MoverDerecha(canvasWidth);
        ReproducirSonido?.Invoke(this, SoundConstants.MovimientoNave);
    }

    public void DispararBala(ICollisionDetector collisionDetector)
    {
        Nave.Disparar(collisionDetector, ReproducirSonido);
    }
}
