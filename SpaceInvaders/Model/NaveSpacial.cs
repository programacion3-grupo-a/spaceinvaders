using SpaceInvaders.Model.Utils;
namespace SpaceInvaders.Model;

public class NaveEspacial : Nave
{
    public int Vidas { get; set; }
    public Bala BalaActual { get; private set; }

    public NaveEspacial(ICanvas canvas, double posicionX, double posicionY, int velocidad, int vidasIniciales, int puntaje)
        : base(canvas, ImageConstants.Jugador, posicionX, posicionY, velocidad, puntaje)
    {
        Vidas = vidasIniciales;
        canvas.MoverImagen(Sprite, PosicionX, PosicionY);
    }

    public void Disparar(ICollisionDetector collisionDetector, EventHandler<String> ReproducirSonido)
    {
        if (BalaActual == null || !BalaActual.EstaViva)
        {
            ReproducirSonido?.Invoke(this, SoundConstants.Disparo);
            double posicionX = PosicionX + (Sprite.ActualWidth - ImageConstants.Bala.Width) / 2;
            double posicionY = PosicionY;
            int velocidadBala = 5;

            BalaActual = new Bala(canvas, collisionDetector, posicionX, posicionY, velocidadBala, TipoBala.NaveEspacial);
            canvas.DibujarImagen(BalaActual.Sprite, BalaActual.PosicionX, BalaActual.PosicionY);
        }
    }

    public void MoverIzquierda(double anchoCanvas)
    {
        PosicionX -= Velocidad;
        LimitarPosicion(anchoCanvas);
        canvas.MoverImagen(Sprite, PosicionX, PosicionY);
    }

    public void MoverDerecha(double anchoCanvas)
    {
        PosicionX += Velocidad;
        LimitarPosicion(anchoCanvas);
        canvas.MoverImagen(Sprite, PosicionX, PosicionY);
    }

    private void LimitarPosicion(double anchoCanvas)
    {
        double limiteIzquierdo = 0;
        double limiteDerecho = anchoCanvas - Sprite.ActualWidth;
        PosicionX = Math.Max(limiteIzquierdo, Math.Min(PosicionX, limiteDerecho));
    }
}
