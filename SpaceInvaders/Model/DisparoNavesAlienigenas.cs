namespace SpaceInvaders.Model;

public class DisparoNavesAlienigenas
{
    private readonly List<Nave> _navesAlienigenas;
    private readonly ICollisionDetector _collisionDetector;

    public DisparoNavesAlienigenas(List<Nave> navesAlienigenas, ICollisionDetector collisionDetector)
    {
        _navesAlienigenas = navesAlienigenas;
        _collisionDetector = collisionDetector;
        IniciarTemporizadorDisparos();
    }

    private void IniciarTemporizadorDisparos()
    {
        var disparoTimer = new DispatcherTimer();
        disparoTimer.Interval = TimeSpan.FromSeconds(1);
        disparoTimer.Tick += DispararNaveAleatoria;
        disparoTimer.Start();
    }

    private void DispararNaveAleatoria(object sender, object e)
    {
        Random rnd = new Random();
        int indiceNave = rnd.Next(0, _navesAlienigenas.Count);
        Nave naveSeleccionada = _navesAlienigenas[indiceNave];

        if (naveSeleccionada is NaveAlienigenaAtacante naveAtacante && naveAtacante.EstaViva)
        {
            naveAtacante.Disparar(_collisionDetector);
        }
    }
}
