namespace SpaceInvaders.Model;

public class NaveAlienigenaAtacante : Nave
{
    public Bala nuevaBala;

    public NaveAlienigenaAtacante(ICanvas canvas, Image sprite, double posicionX, double posicionY, double velocidad, int puntos)
        : base(canvas, sprite, posicionX, posicionY, velocidad, puntos)
    {
    }

    public void Disparar(ICollisionDetector collisionDetector)
    {
        double posicionXbala = PosicionX + 2;
        double posicionYbala = PosicionY + 22;
        int velocidadBala = 5;

        nuevaBala = new Bala(canvas, collisionDetector, posicionXbala, posicionYbala, velocidadBala, TipoBala.NaveAlienigena);
        canvas.DibujarImagen(nuevaBala.Sprite, nuevaBala.PosicionX, nuevaBala.PosicionY);
    }
}
