namespace SpaceInvaders.Model;

public interface IMovible
{
    void Mover();
    void Dibujar();
    void Eliminar();
    bool VerificarColisiones(double balaLeft, double balaTop, double balaWidth, double balaHeight);
}
