using SpaceInvaders.Model.Utils;

namespace SpaceInvaders.Model;
public class NaveAlienigenaRoja : Nave
{
    private Random generadorAleatorio;
    private double direccionX;
    private bool esperandoMovimiento;
    private DispatcherTimer naveTimer;

    private const int PuntuacionMinima = 10;
    private const int PuntuacionMaxima = 200;
    private const int TiempoEsperaInicial = 10000;
    private const int LadoMargenCanvas = 5;
    private const int posicionInicialY = 65;
    public int TiempoAparicion { get; set; }

    public NaveAlienigenaRoja(ICanvas canvas, double posicionInicialX, double velocidad)
        : base(canvas, ImageConstants.NaveRoja, posicionInicialX, posicionInicialY, velocidad,0)
    {
        generadorAleatorio = new Random();
        TiempoAparicion = TiempoEsperaInicial;
        direccionX = generadorAleatorio.Next(0, 2) == 0 ? -1 : 1;
        esperandoMovimiento = false;

        naveTimer = new DispatcherTimer();
        naveTimer.Interval = TimeSpan.FromMilliseconds(160);
        naveTimer.Tick += NaveAlienigenaRoja_Tick;
        naveTimer.Start();
    }

    private void NaveAlienigenaRoja_Tick(object sender, object e)
    {
        MoverNaveRoja();
    }

    public async void MoverNaveRoja()
    {
        if (!esperandoMovimiento)
        {
            PosicionX += direccionX * Velocidad;

            if (PosicionX < 0 || PosicionX + Sprite.ActualWidth > (canvas.CanvasAncho - LadoMargenCanvas))
            {
                InvertirDireccionX();
                esperandoMovimiento = true;
                Sprite.Visibility = Visibility.Collapsed;
                naveTimer.Stop();

                await Task.Delay(TiempoAparicion).ConfigureAwait(true);
                Sprite.Visibility = Visibility.Visible;
                EstaViva = true;
                esperandoMovimiento = false;
                Puntos = generadorAleatorio.Next(PuntuacionMinima, PuntuacionMaxima + 1);
                naveTimer.Start();
            }
            Mover();
        }
    }

    private void InvertirDireccionX()
    {
        direccionX *= -1;
    }
}
