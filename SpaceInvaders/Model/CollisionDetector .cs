namespace SpaceInvaders.Model;
using SpaceInvaders.Model.Utils;

public class CollisionDetector : ICollisionDetector
{
    public event EventHandler<int> VidasActualizadas;
    public event EventHandler<int> PuntajeActualizado;
    public event EventHandler<String> ReproducirSonido;
    private List<Muro> muros;
    private List<Nave> navesEnemigas;
    private NaveAlienigenaRoja naveAlienigenaRoja;
    private NaveEspacial naveEspacial;

    public CollisionDetector(List<Muro> muros, List<Nave> navesEnemigas, NaveAlienigenaRoja naveAlienigenaRoja, NaveEspacial naveEspacial)
    {
        this.muros = muros;
        this.navesEnemigas = navesEnemigas;
        this.naveAlienigenaRoja = naveAlienigenaRoja;
        this.naveEspacial = naveEspacial;
        VidasActualizadas?.Invoke(this, naveEspacial.Vidas);
    }

    public bool DetectarColision(TipoBala origen, double balaLeft, double balaTop, double balaWidth, double balaHeight)
    {
        if (origen == TipoBala.NaveAlienigena)
        {
            foreach (var muro in muros)
            {
                if (muro.ColisionConBala(balaLeft, balaTop, balaWidth, balaHeight, false))
                {
                    return true;
                }
            }

            if (naveEspacial.EstaViva && naveEspacial.VerificarColisiones(balaLeft, balaTop, balaWidth, balaHeight))
            {
                naveEspacial.Vidas -= 1;
                VidasActualizadas?.Invoke(this, naveEspacial.Vidas);
                return true;
            }
        }
        else if(origen == TipoBala.NaveEspacial)
        {
            foreach (var muro in muros)
            {
                if (muro.ColisionConBala(balaLeft, balaTop, balaWidth, balaHeight, true))
                {
                    ReproducirSonido?.Invoke(this, SoundConstants.Explosion);
                    return true;
                }
            }

            foreach (var naveEnemiga in navesEnemigas)
            {
                if (naveEnemiga.EstaViva && naveEnemiga.VerificarColisiones(balaLeft, balaTop, balaWidth, balaHeight))
                {
                    naveEspacial.Puntos += naveEnemiga.Puntos;
                    PuntajeActualizado?.Invoke(this, naveEspacial.Puntos);
                    ReproducirSonido?.Invoke(this, SoundConstants.ExplosionNave);
                    navesEnemigas.Remove(naveEnemiga);
                    naveEnemiga.Eliminar();
                    return true;
                }
            }

            if (naveAlienigenaRoja.EstaViva && naveAlienigenaRoja.VerificarColisiones(balaLeft, balaTop, balaWidth, balaHeight))
            {
                naveEspacial.Puntos += naveAlienigenaRoja.Puntos;
                PuntajeActualizado?.Invoke(this, naveEspacial.Puntos);
                ReproducirSonido?.Invoke(this, SoundConstants.Explosion);
                naveAlienigenaRoja.EstaViva = false;
                naveAlienigenaRoja.Sprite.Visibility = Visibility.Collapsed;
                return true;
            }
        }
        return false;
    }

    public bool DetectarColisionNaves(double naveLeft, double naveTop, double naveWidth, double naveHeight)
    {
        if (naveEspacial.EstaViva && naveEspacial.VerificarColisiones(naveLeft, naveTop, naveWidth, naveHeight))
        {
            ReproducirSonido?.Invoke(this, SoundConstants.Explosion);
            return true;
        }
        return false;
    }
}
