namespace SpaceInvaders.Model;

public interface ICollisionDetector
{
    event EventHandler<int> VidasActualizadas;
    event EventHandler<int> PuntajeActualizado;
    event EventHandler<String> ReproducirSonido;
    bool DetectarColision(TipoBala origen, double balaLeft, double balaTop, double balaWidth, double balaHeight);
    bool DetectarColisionNaves(double naveLeft, double naveTop, double naveWidth, double naveHeight);
}
