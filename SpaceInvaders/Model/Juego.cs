using SpaceInvaders.Model.Utils;
namespace SpaceInvaders.Model;

public class Juego
{
    public NaveAlienigenaRoja NaveAlienigenaRoja { get; }
    public List<Muro> Muros { get; }
    public List<Nave> NavesAlienigenas { get; }
    public ICanvas canvas;
    public ICollisionDetector collisionDetector;
    public DisparoNavesAlienigenas disparoNavesAlienigenas;
    public MovimientoNavesAlienigenas movimientoNavesAlienigenas;
    public int velocidadNavesAlienigenas = 5;

    public Juego(ICanvas canvas)
    {
        this.canvas = canvas;
        Muros = new List<Muro>();
        DistribuirMurosEnCanvas();

        int posicionInicialX = 0;
        NaveAlienigenaRoja = new NaveAlienigenaRoja(canvas, posicionInicialX, velocidadNavesAlienigenas);
        canvas.DibujarImagen(NaveAlienigenaRoja.Sprite, NaveAlienigenaRoja.PosicionX, NaveAlienigenaRoja.PosicionY);

        NavesAlienigenas = new List<Nave>();
        DistribuirNavesAlienigenas();
    }

    public ICollisionDetector CollisionDetector
    {
        set { collisionDetector = value;
            IniciarTemporizadorDisparos();
        }
    }

    private void IniciarTemporizadorDisparos()
    {
        disparoNavesAlienigenas = new DisparoNavesAlienigenas(NavesAlienigenas, collisionDetector);
        movimientoNavesAlienigenas = new MovimientoNavesAlienigenas(canvas, NavesAlienigenas, collisionDetector);
        movimientoNavesAlienigenas.NuevaOleadaGenerada += GenerarNuevaOleada;
    }

    private void DistribuirNavesAlienigenas()
    {
        DistribuirNavesEnFila(1, 0, 10, 110);
        DistribuirNavesEnFila(2, 1, 10, 110);
        DistribuirNavesEnFila(2, 2, 10, 140);
    }

    private void DistribuirNavesEnFila(int cantidadFilas, int filaBase, int columnas, double posY)
    {
        double espacioEntreNaves = 35;
        double posicionInicialX = 45;

        for (int fila = 0; fila < cantidadFilas; fila++)
        {
            for (int columna = 0; columna < columnas; columna++)
            {
                double posX = (columna * espacioEntreNaves) + posicionInicialX;
                double posicionY = posY + (fila + filaBase) * espacioEntreNaves;

                Nave nuevaNave = null;
                if (filaBase == 0)
                {
                    nuevaNave = new NaveAlienigenaAtacante(canvas, ImageConstants.NaveEnemigaAtacante, posX, posicionY, velocidadNavesAlienigenas, 40);
                }
                else if (filaBase == 1)
                {
                    nuevaNave = new NaveAlienigena(canvas, ImageConstants.NaveEnemiga10, posX, posicionY, velocidadNavesAlienigenas, 10);
                }
                else if (filaBase == 2)
                {
                    nuevaNave = new NaveAlienigena(canvas, ImageConstants.NaveEnemiga20, posX, posicionY, velocidadNavesAlienigenas, 20);
                }
                NavesAlienigenas.Add(nuevaNave);
                nuevaNave.Dibujar();
            }
        }
    }

    public void DistribuirMurosEnCanvas()
    {
        int posicionInicialX = 0;
        int distanciaEntreMuros = 160;
        int posicionY = 350;
        int cantidadMuros = 4;

        for (int i = 0; i < cantidadMuros; i++)
        {
            Muro nuevoMuro = new Muro(canvas);
            nuevoMuro.PosicionX = posicionInicialX + i * distanciaEntreMuros;
            nuevoMuro.PosicionY = posicionY;
            Muros.Add(nuevoMuro);
            nuevoMuro.DibujarMuros();
        }
    }

    private void GenerarNuevaOleada(object sender, EventArgs e)
    {
        DistribuirNavesAlienigenas();
    }
}
