using SpaceInvaders.Model.Utils;
namespace SpaceInvaders.Model;

internal class ImagenManagerBloque
{
    private List<Image> murosDestruidosJugador;
    private List<Image> murosDestruidosEnemigo;

    public ImagenManagerBloque()
    {
        murosDestruidosJugador = new List<Image>
        {
            ImageConstants.JugadorBloque_1,
            ImageConstants.JugadorBloque_2,
            ImageConstants.JugadorBloque_3,
            ImageConstants.JugadorBloque_4,
        };
        murosDestruidosEnemigo = new List<Image>
        {
            ImageConstants.EnemigoBloque_1,
            ImageConstants.EnemigoBloque_2,
            ImageConstants.EnemigoBloque_3,
            ImageConstants.EnemigoBloque_4,
        };
    }

    public Image ObtenerImagenDestruida(int resistencia, bool esJugador)
    {
        if (esJugador)
        {
            return murosDestruidosJugador[5 - resistencia];
        }
        else
        {
            return murosDestruidosEnemigo[5 - resistencia];
        }
    }
}
