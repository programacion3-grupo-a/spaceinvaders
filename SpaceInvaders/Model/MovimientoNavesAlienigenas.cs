namespace SpaceInvaders.Model;
using SpaceInvaders.Model.Utils;

public class MovimientoNavesAlienigenas
{
    private readonly ICollisionDetector _collisionDetector;
    private readonly List<Nave> _navesAlienigenas;
    private DispatcherTimer movimientoTimer;
    private readonly ICanvas _canvas;
    private int _direccionX = 1;

    public MovimientoNavesAlienigenas(ICanvas canvas, List<Nave> navesAlienigenas, ICollisionDetector collisionDetector)
    {
        _navesAlienigenas = navesAlienigenas;
        _collisionDetector = collisionDetector;
        _canvas = canvas;
        IniciarTemporizadorMovimientos();
    }

    private void IniciarTemporizadorMovimientos()
    {
        movimientoTimer = new DispatcherTimer();
        movimientoTimer.Interval = TimeSpan.FromMilliseconds(400);
        movimientoTimer.Tick += (sender, e) => MoverNaves();
        movimientoTimer.Start();
    }

    public void MoverNaves()
    {
        bool cambiarDireccion = false;
        bool hayNavesVivas = false;
        ReproducirSonido?.Invoke(this, SoundConstants.MovimientoNave);
        foreach (var nave in _navesAlienigenas)
        {
            if (nave.EstaViva)
            {
                hayNavesVivas = true;
                nave.PosicionX += _direccionX * nave.Velocidad;
                nave.Mover();

                if (nave.PosicionX + _direccionX * nave.Velocidad + nave.Sprite.ActualWidth > _canvas.CanvasAncho || nave.PosicionX + _direccionX * nave.Velocidad < 0)
                {
                    cambiarDireccion = true;
                }

                if (_collisionDetector.DetectarColisionNaves(nave.PosicionX, nave.PosicionY, nave.Sprite.ActualWidth, nave.Sprite.ActualHeight))
                {
                    movimientoTimer.Stop();
                    OnTerminarJuegoRequested();
                }
            }
        }

        if (!hayNavesVivas)
        {
            OnNuevaOleadaGenerada();
        }

        if (cambiarDireccion)
        {
            _direccionX *= -1;
            DescenderNaves();
        }
    }

    private void DescenderNaves()
    {
        double distanciaY = 20;
        foreach (var nave in _navesAlienigenas)
        {
            if (nave.EstaViva)
            {
                nave.Velocidad += 3;
                nave.PosicionY += distanciaY;
                nave.Mover();
            }
        }
    }

    public event EventHandler NuevaOleadaGenerada;
    public event EventHandler TerminarJuegoRequested;
    public event EventHandler<String> ReproducirSonido;

    private void OnNuevaOleadaGenerada()
    {
        NuevaOleadaGenerada?.Invoke(this, EventArgs.Empty);
    }

    private void OnTerminarJuegoRequested()
    {
        TerminarJuegoRequested?.Invoke(this, EventArgs.Empty);
    }
}
