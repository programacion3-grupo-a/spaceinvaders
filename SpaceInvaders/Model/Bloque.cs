namespace SpaceInvaders.Model;

public class Bloque
{
    public Image Imagen { get; private set; }
    public int Resistencia { get;  set; }

    private ImagenManagerBloque ImagenManagerBloque;

    public Bloque(Image imagen, int resistencia)
    {
        Imagen = imagen;
        Resistencia = resistencia;
        ImagenManagerBloque = new ImagenManagerBloque();
    }

    public bool ReducirResistencia(bool esJugador)
    {
        if (Resistencia > 1)
        {
            Image imagenDestruida = ImagenManagerBloque.ObtenerImagenDestruida(Resistencia, esJugador);
            Imagen.Source = imagenDestruida.Source;
            Resistencia--;
            return false;
        }

        return true; 
    }
}
