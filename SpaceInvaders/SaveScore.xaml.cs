using SpaceInvaders.Model;

namespace SpaceInvaders;

public sealed partial class SaveScore : Page
{
    private int puntajeNaveEspacial;

    public SaveScore()
    {
        this.InitializeComponent();
    }

    protected override void OnNavigatedTo(NavigationEventArgs e)
    {
        base.OnNavigatedTo(e);
        if (e.Parameter != null)
        {
            puntajeNaveEspacial = (int)e.Parameter;
            score.Text = puntajeNaveEspacial.ToString();
        }
    }

    private void Guardar_Click(object sender, RoutedEventArgs e)
    {
        string playerName = UserNameTextBox.Text;
        if (!string.IsNullOrEmpty(playerName))
        {
            ScoreManager.SaveScore(playerName, score.Text);
            UserNameTextBox.Text = "";
            score.Text = "";
        }
    }

    private void VolverAJugar_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(MainPage));
    }

    private void RegresarAtras_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(InicioSesion));
    }
}
